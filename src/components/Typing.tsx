import React from 'react';

const Typing = (props: any) => {
  return (
    <div className={"message-container message-assistant"}>
      <div className="message-lines">
            <div className="message-line">
              <div className="message-text">
                <div className="d-block">Escribiendo...</div>
              </div>
            </div>
      </div>
      <div className="message-who">@Watson Assistant</div>
    </div>
  )
}

export default Typing;