import React, { useState } from 'react';
import Recorder from './Recorder';
import teclado from '../assets/teclado.png';
import './ChatOptions.css';

const ChatOptions = (props: any) => {
  const [input, setInput] = useState("");
  const [keyboard, setKeyboard] = useState(false);
  return (
    <div className="App-chatactions border-top p-3 position-relative">
      <div className="toggleKeyboard" onClick={() => setKeyboard(!keyboard)}>
        <img src={teclado} height="24" width="24" />
      </div>
      { keyboard ? 
        <input 
          type="text" 
          placeholder="Escribe aqui tu pregunta..." 
          onChange={ e => setInput(e.target.value) }
          onKeyDown={ e => {
            if (e.key === 'Enter') {
              props.submitQuestion(input);
              setInput("");
            }
          }}
          value={input} /> 
      : 
        <Recorder submitQuestion={props.submitQuestion} /> 
      }
    </div>   
  );
}

export default ChatOptions;