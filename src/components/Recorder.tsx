import React, { useState } from 'react';
import { ReactMic } from 'react-mic';
import { Mic, MicOff } from 'react-feather';
import SpeechToText from '../services/SpeechToText';
import './Recorder.css';

const Recorder = (props: any) => {
  const [record, setRecord] = useState(false);
  const s2t = new SpeechToText();

  const recorder = () => {
    setRecord(!record)
  }

  const onStop = (recordedBlob: any) => {
    const fd = new FormData();
    fd.append("audio", recordedBlob.blob);
    s2t.recognize(fd).then( response => {
      const submitQuestion = props.submitQuestion;
      const transcript = response.results[0].alternatives[0].transcript;
      submitQuestion(transcript, recordedBlob.blobURL);
    } )
  }

  return(
    <div className="recorder-container">
      <ReactMic
        record={record}
        className="sound-wave"
        onStop={onStop}
        strokeColor="#9579e7"
        backgroundColor="transparent" />
      <button className="buttonRecorder" onClick={recorder} type="button">
        { record ? <MicOff size={20} /> : <Mic size={20} /> }
      </button>
    </div>
  )
}

export default Recorder;