import React from 'react';
import './Message.css';

const Message = (props: any) => {
  const message = props.message;
  const who = message.who == "assistant" ? "Watson Assistant" : "Usuario";
  return(
    <div className={"message-container message-" + message.who}>
      <div className="message-lines">
        { message.generic.map( (item: any, index: number) => {
          console.log(item);
          return (
            <div className="message-line" key={index}>
              <div className="message-text">
                <div className="d-block">{ item.text }</div>
              </div>
              { item.audio_url && (
              <div className="message-audio">
                <audio controls>
                  <source src={item.audio_url} type="audio/ogg" />
                </audio> 
              </div>
              )}
            </div>
          )
        } ) }
      </div>
      <div className="message-who">@{who}</div>
    </div>
  )
}

export default Message;