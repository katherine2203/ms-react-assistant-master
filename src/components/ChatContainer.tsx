import React, { PureComponent } from 'react';
import Message from './Message';
import Typing from './Typing';

class ChatContainer extends PureComponent<{
  messages: any,
  typing: boolean
}> {
  private el: any;

  componentDidMount() {
    this.scrollToBottom();
  }

  componentDidUpdate() {
    this.scrollToBottom();
  }

  scrollToBottom() {
    this.el.scrollIntoView({ behavior: 'smooth' });
  }
  
  render() {
    const { messages, typing } = this.props;
    return (
      <div className="App-chatcontent p-3">
        { messages && messages.map( (message: any) => {
          return (
            <Message key={message.id} message={message} />
          )
        } ) }
        { typing && <Typing /> }
        <div ref={el => { this.el = el; }}></div>
      </div>  
    )
  }
};

export default ChatContainer;