class TextToSpeech {
  private url: string;

  constructor() {
    this.url = "https://ms-text-to-speech.us-south.cf.appdomain.cloud";
  }

  synthesize = async(text: string) => {
    let response = await fetch(`${this.url}/synthesize`,
			{
        method: 'POST',
        headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json',
				},
				body: JSON.stringify({
          text: text
        })
			}
		);
		let responseJson = await response.json();
		return responseJson;
  }

  getAudio = (path: string) => {
    return `${this.url}/${path}`;
  }
}

export default TextToSpeech;