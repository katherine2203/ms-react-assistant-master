class SpeechToText {
  private url: string;

  constructor() {
    this.url = "https://ms-speech-to-text.us-south.cf.appdomain.cloud";
  }

  recognize = async(formData: any) => {
    let response = await fetch(`${this.url}/recognize`,
			{
				method: 'POST',
				body: formData
			}
		);
		let responseJson = await response.json();
		return responseJson;
  }
}

export default SpeechToText;