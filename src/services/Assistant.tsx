class Assistant {
	private url: string;

	constructor() {
		this.url = `https://ms-assistant.us-south.cf.appdomain.cloud`;
  }

	public createSession = async() => {
		let response = await fetch(`${this.url}/create_session`, { method: 'POST' });
		let responseJson = await response.json();
    return responseJson;
  }

	public sendMessage = async(session_id: string, text: string) => {
		let response = await fetch(`${this.url}/message`,
			{
				method: 'POST',
				headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json',
				},
				body: JSON.stringify({
					text: text,
					session_id: session_id
				})
			}
		);
		let responseJson = await response.json();
		return responseJson;
  };
}

export default Assistant;