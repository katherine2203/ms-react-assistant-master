import React, { PureComponent } from 'react';
import './App.css';
import logo from './assets/watson.svg';
import Assistant from './services/Assistant';
import Message from './components/Message';
import ChatOptions from './components/ChatOptions';
import TextToSpeech from './services/TextToSpeech';
import uuid from 'uuid';
import ChatContainer from './components/ChatContainer';

class App extends PureComponent {
  private assistant = new Assistant();
  private textToSpeech = new TextToSpeech();
  
  readonly state = {
    messages: [],
    session_id: "",
    typing: false
  };
  
  componentDidMount = async() => {
    const session = await this.assistant.createSession();
    this.setState({
      session_id: session.session_id
    }, () => this.submitQuestion(""));
  }
   
  submitQuestion = (input: string, audio: any = undefined) => {
    if( !this.state.typing ) {
      if( input ) {
        this.messageUser(input, audio);
      }
      this.setState({
        typing: true
      }, () => {
        this.messageAssistant(input);
      })
    }
  };

  messageUser = (input: string, audio: any = undefined) => {
    const answer = { generic: [ { response_type: "text", text: input, audio_url: audio } ], who: "user", id: uuid.v4() }
    this.setState( {
      messages: [ ...this.state.messages, answer ],
      typing: false
    } )
  }

  messageAssistant = async (input: string) => {
    const message = await this.assistant.sendMessage(this.state.session_id, input);
    const answer = message.output;
    answer.who = "assistant";
    answer.id = uuid.v4();

    if( answer.generic && answer.generic.length > 0) {
      let count = 0;
      answer.generic.forEach(async(item: any, index: number) => {
        const key = index + 1;
        const speech = await this.textToSpeech.synthesize(item.text);
        answer.generic[index].audio_url = this.textToSpeech.getAudio(speech.audio);
        count++;
        if( count == key ) {
          this.setState( {
            messages: [ ...this.state.messages, answer ],
            typing: false
          } );
        }
      });
    } else {
      this.setState( {
        messages: [ ...this.state.messages, answer ],
        typing: false
      } );
    }
  }
  
  render() {
    return (
      <div className="App vh-100 vw-100 d-flex flex-column">
        <header className="App-header bg-dark px-3 py-3">
          <img className="my-2" src={logo} height="15" />
        </header>
        <section className="App-section flex-fill d-flex flex-row">
          <div className="App-chat h-100 w-50 border-right d-flex flex-column">
            <ChatContainer typing={this.state.typing} messages={this.state.messages} />
            <ChatOptions typing={this.state.typing} messageUser={this.messageUser} submitQuestion={this.submitQuestion} />
          </div>
          <div className="App-content w-50 h-100">

          </div>
        </section>
      </div>
    );
  }
}

export default App;
